<?php

return [
	'button' => [
		'go_back' => 'overzicht',
		'page_next' => 'volgende',
		'page_prev' => 'vorige',
	],
	'form' => [
		'email_is_invalid' => 'dit is geen geldig emailadres',
		'required' => 'verplicht veld',
	],
	'headers' => [
		'related_docs' => 'Gerelateerde documenten',
	],
];
