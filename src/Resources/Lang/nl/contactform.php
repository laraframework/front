<?php

return [
	'button' => [
		'submit' => 'versturen',
	],
	'formfield' => [
		'comment' => 'opmerking',
		'email' => 'Email',
		'name' => 'Naam',
	],
];
