<?php

return [
	'button' => [
		'submit' => 'send',
	],
	'formfield' => [
		'comment' => 'comment',
		'email' => 'Email',
		'name' => 'Name',
	],
];
