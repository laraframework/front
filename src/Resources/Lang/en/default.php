<?php

return [
	'button' => [
		'go_back' => 'back',
		'page_next' => 'next',
		'page_prev' => 'previous',
	],
	'form' => [
		'email_is_invalid' => 'this is not a valid email address',
		'required' => 'required field',
	],
	'headers' => [
		'related_docs' => 'Related pages',
	],
];
