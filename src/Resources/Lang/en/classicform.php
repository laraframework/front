<?php

return [
	'button' => [
		'submit' => 'submit',
	],
	'formfield' => [
		'email' => 'email',
		'name' => 'name',
	],
];
